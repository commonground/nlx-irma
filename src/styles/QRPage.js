
const qrpStyles = theme => ({
	root:{
    display:'flex',
    justifyContent:'center',
    flexDirection:'column',
    alignItems:'center',
    //center 
    height: 'calc(100% - 100px)'
  },
  title:{
    fontColor: theme.palette.primary 
  }
});

export default qrpStyles;