import React, { Component } from 'react';

class NoCompany extends Component {
  render() {
    return (
      <div>
        <h1>Please select company from the list</h1>
      </div>
    );
  }
}

export default NoCompany;